#!/usr/bin/bash

source .env

RESTIC_BIN=${RESTIC_BIN:-/usr/bin/restic}
export RESTIC_REPOSITORY=${RESTIC_REPOSITORY:-"$PWD/repository"}
export RESTIC_PASSWORD=${RESTIC_PASSWORD:-supersecret}

colblk='\033[0;30m' # Black - Regular
colred='\033[0;31m' # Red
colgrn='\033[0;32m' # Green
colylw='\033[0;33m' # Yellow
colpur='\033[0;35m' # Purple
colblu='\033[0;34m' # Blue
colwht='\033[0;97m' # White
colinvert='\e[7m'   # Invert
colrst='\033[0m'    # Text Reset

verbosity=3

### verbosity levels
crt_lvl=1
err_lvl=2
wrn_lvl=3
inf_lvl=4
dbg_lvl=5

function warn ()  { verb_lvl=$wrn_lvl log "${colylw}[WARNING]${colrst} $@" ;}
function info ()  { verb_lvl=$inf_lvl log "${colgrn}[INFO]${colrst} $@" ;}
function debug () { verb_lvl=$dbg_lvl log "${colblu}[DEBUG]${colrst} $@" ;}
function error () { verb_lvl=$err_lvl log "${colred}[ERROR]${colrst} $@" ;}
function crit ()  { verb_lvl=$crt_lvl log "${colpur}[FATAL]${colrst} $@" ;}
function log() {
  if [ $verbosity -ge $verb_lvl ]; then
    datestring=`date +"%Y-%m-%d %H:%M:%S"`
    echo -e "$datestring $@"
  fi
}

function split_line ()  {
  local size=100
  local text=$1
  if [ -z "$text" ]; then
    echo -e "${colinvert}$(seq -s= ${size}|tr -d '[:digit:]')${colrst}"
    return 0
  fi
  local text_size=${#text}
  local left=$(( ($size - $text_size)/2 - 1 ))
  local right=$(( $size - $left - $text_size - 1 ))
  echo -e -n "${colinvert}$(seq -s= ${left}|tr -d '[:digit:]')"
  echo -e -n " ${text} "
  echo -e "$(seq -s= ${right}|tr -d '[:digit:]')${colrst}"
}

run_function() {
  local start=$(date +%s%N)
  local fn=$1
  shift 1
  debug "Begin $fn function"
  eval "$fn" "$@"
  local end=$(date +%s%N)
  local duration=$(( (end - start) / 1000000 ))
  debug "Exiting $fn function with ${duration} ms"
}

check_requirements() {
  for soft in $RESTIC_BIN parallel date tar echo sed awk head tail; do
    if ! command -v $soft &> /dev/null; then
      crit "$soft could not be found"
      exit 1
    fi
  done
  debug "All the requirements have been satisfied"
}

repo_init() {
  if [ ! -d $RESTIC_REPOSITORY ]; then
    debug "Restic repository cannot be found"
    info "Initializing restic repository on $RESTIC_REPOSITORY"
    if $RESTIC_BIN init > /dev/null; then
      info "Restic repository has been successfully initialised on $RESTIC_REPOSITORY"
    else
      crit "Failed to initialised the restic repository on $RESTIC_REPOSITORY"
    fi
  fi
}

repo_check() {
  if ! $RESTIC_BIN check > /dev/null; then
    crit "The repository $RESTIC_REPOSITORY is corrupt"
    exit 1
  fi
}

display_debugging() {
  echo "Context:"
  echo -e "\t- Current directory: $PWD"
  echo -e "\t- Restic binary: $RESTIC_BIN"
  echo "Variables:"
  echo -e "\t- RESTIC_REPOSITORY: $RESTIC_REPOSITORY"
  echo -e "\t- RESTIC_PASSWORD: $RESTIC_PASSWORD"
}

repo_list_snapshots() {
  local output="$($RESTIC_BIN snapshots --quiet)"
  if [ -z "$output" ]; then
    echo "There is no snapshot in this repository"
    return 0
  fi
  echo "$output"
}

repo_create_snapshot() {
  local list=()
  for folder in $(echo $FOLDERS_TO_SAVE | tr '|' ' '); do
    local absolute="${PARENT_FOLDER}/${folder}"
    if [ ! -d $absolute ]; then
      warn "Folder $absolute doesn't exist"
    fi
    list+=("${PARENT_FOLDER}/${folder}")
  done

  if ! $RESTIC_BIN backup --with-atime "${list[@]}"; then
    crit "Failed to backup..."
    exit 1
  fi
}

prepare_working_directory_compare() {
  rm -rf "${WORKING_DIRECTORY_COMPARE}/*"
  mkdir -p "${WORKING_DIRECTORY_COMPARE}/${PARENT_FOLDER}"
}

create_patch_and_clean() {
  local cur_dir="$1"
  local patch_filename="$2"

  # Create patch archive
  cd "${WORKING_DIRECTORY_COMPARE}/${PARENT_FOLDER}"
  local patch_dst="${cur_dir}/${patch_filename}"
  tar -c -z -f "${patch_dst}" --totals .
  echo "Creating a patch ${patch_dst}"
  cd "${cur_dir}"

  # Clean
  rm -rf "${WORKING_DIRECTORY_COMPARE}/"
}

repo_create_patch() {
  local output="$($RESTIC_BIN snapshots --compact --quiet | tail -n +3 | head -n -3 | awk '{ print $1 "|" $2 "|" $3 }')"

  # It's the first patch!
  if [ -z "$output" ]; then
    if ! [[ $($RESTIC_BIN snapshots --compact --quiet) ]]; then
      crit "You need to create a snapshot first!"
      return 1
    fi
    local first_snap="$($RESTIC_BIN snapshots --compact --quiet | tail -n +3 | head -n -2 | awk '{ print $1 }')"
    prepare_working_directory_compare
    $RESTIC_BIN restore "${first_snap}" --target "${WORKING_DIRECTORY_COMPARE}"

    local cur_dir="$PWD"
    local script_file="${cur_dir}/apply_full_${first_snap}.bash"
    local patch_filename="full_${first_snap}.tar.gz"

  cat <<EOF > "${script_file}"
#!/usr/bin/bash
#
# Creation date: $(date '+%F %H:%M:%S')
# First patch: $first_snap
#

while true; do
  echo -n "Enter destination folder: "
  read
  DESTINATION="\$REPLY"
  if [ ! -d "\$DESTINATION" ]; then
    echo "This folder doesn't exists"
    continue
  fi
  exit=0
  for folder in $FOLDERS_TO_SAVE; do
    absolute_folder="\$DESTINATION/\$folder"
    if [ -d "\$absolute_folder" ]; then
      echo "You need to delete this folder '\$absolute_folder' to apply this patch"
      exit=1
    fi
  done
  if [ "\$exit" == 1 ]; then
    exit 1
  fi
  break
done

METADATA_FILE="\$DESTINATION/.metadata"

tar -x -z -f ${patch_filename} --totals -C "\$DESTINATION"

echo "$first_snap - patch created on $(date '+%F %H:%M:%S') and applied on \$(date '+%F %H:%M:%S')" > "\${METADATA_FILE}"

EOF

    create_patch_and_clean "${cur_dir}" "${patch_filename}"
    return 0
  fi

  # Create a list of snapshot expect the last one
  local list=()
  for line in $output; do
    local item="$(echo $line | cut -d '|' -f 1) ($(echo $line | cut -d '|' -f 2,3 | tr '|' ' '))"
    list+=("$item")
  done
  list+=("Back to main menu")

  PS3="From which snapshot you want a patch? "
  select choice in "${list[@]}"
  do
    case $choice in
      "Back to main menu")
        return 0;;
      '')
        echo "Try again";;
      *)
        patch_from_snap="$(awk '{ print $1 }' <<< $choice)"
        break;;
    esac
  done

  echo "You choose ${patch_from_snap} snapshot"

  local last_snap=$($RESTIC_BIN snapshots --compact --quiet | tail -n 3 | head -n 1 | awk '{ print $1 }')

  echo "Creating a patch from ${patch_from_snap} to ${last_snap}"
  run_function repo_compare_snapshots "${patch_from_snap}" "${last_snap}"
}

repo_compare_snapshots() {
  local before="$1"
  local after="$2"
  local files="$($RESTIC_BIN diff --metadata $before $after | tail -n +3 | sed '1,/^$/!d' | sed '$ d')"
  
  # Preparing destination folder
  prepare_working_directory_compare

  # Extract added, modified files
  local actions=()
  for file in $(grep -vE '^-|^U' <<< $files | awk '{ print $2 }'); do
    echo "Restoring $file"
    if grep -q -E '/$' <<< $file; then
      # This is a folder restic dump output tar file
      actions+=("$RESTIC_BIN dump --archive tar $after $file | tar -x -C "${WORKING_DIRECTORY_COMPARE}"")
    else
      local destination="${WORKING_DIRECTORY_COMPARE}/$file"
      mkdir -p "$(dirname $destination)"
      actions+=("$RESTIC_BIN dump $after $file > $destination")
    fi
  done
  
  printf '%s\n' "${actions[@]// / }" | parallel --progress -j $(( nproc / 2 )) "{}"

  if [ $? -ne 0 ]; then
    crit "Failed to create this patch! Maybe retry"
    exit 1
  fi
  
  local cur_dir=$PWD
  local patch_filename="incremental_${before}_${after}.tar.gz"
  local script_file="${cur_dir}/apply_${before}_${after}.bash"

  cat <<EOF > "${script_file}"
#!/usr/bin/bash
# Creation date: $(date '+%F %H:%M:%S')
# Patch between: $before - $after
#
# Check if the last snapshot applied is the snapshot from of this patch

while true; do
  echo -n "Enter destination folder: "
  read
  METADATA_FILE="\${REPLY}/.metadata"
  if [ -f "\${METADATA_FILE}" ]; then
    snap_now="\$(cat "\${METADATA_FILE}" | awk '{ print \$1 }')"
    if [ "${before}" != "\${snap_now}" ]; then
      echo "You cannot apply this patch! You need to apply a patch from \${snap_now}"
      exit 1
    fi
    break;
  else
    echo "This folder cannot be the destination"
  fi
done

DESTINATION="\$(dirname "\${METADATA_FILE}")"

tar -x -z -f ${patch_filename} --totals -C "\$DESTINATION"

EOF

  # Extract removed files
  for file in $(grep -E '^-' <<< $files | awk '{ print $2 }'); do
    local relative_file="$(sed "s|^$PARENT_FOLDER||" <<< $file)"
    echo "/usr/bin/rm -rf \"\$DESTINATION/${relative_file}\"" >> "${script_file}"
  done

cat <<EOF >> "${script_file}"

FILES="$(${RESTIC_BIN} ls ${after} --quiet --recursive | sed "s|^${PARENT_FOLDER}||" | grep -vE '^/')"

# Check only list of files no checksum
while read file; do
  if [ "\$file" == "\$DESTINATION/.metadata" -o "\$file" == "\$DESTINATION" ]; then
    continue
  fi
  if ! grep -q -E "^\$(sed "s|^\${DESTINATION}/||" <<< \${file})$" <<< \$FILES; then
    echo "CRITICAL - Your destination is not compliant! The '\$file' file/folder should not be present!"
  fi
done < <(find \$DESTINATION | sed 's|^\$DESTINATION||')

for file in \$FILES; do
  if [ ! -e "\$DESTINATION/\$file" ]; then
    echo "CRITICAL - Your destination is not compliant! The '\$DESTINATION/\$file' file/folder is missing!"
  fi
done

echo "$after - patch created on $(date '+%F %H:%M:%S') and applied on \$(date '+%F %H:%M:%S')" > "\${METADATA_FILE}"
EOF

  create_patch_and_clean "${cur_dir}" "${patch_filename}"
}

repo_show_stats() {
  $RESTIC_BIN stats latest --quiet
}

repo_prune_six_months() {
  $RESTIC_BIN forget --keep-within 6m
}

advanced_menu() {
  while true; do
    split_line
    PS3="Select your answer: "
    select choice in "List snapshots" "Show statistics" "Delete snapshots older than 6 months" "Display debugging context" "Back to main menu" "Quit"
    do
      split_line
      case $choice in
        "List snapshots")
          run_function repo_list_snapshots
          break;;
        "Display debugging context")
          run_function display_debugging
          break;;
        "Show statistics")
          run_function repo_show_stats
          break;;
        "Delete snapshots older than 6 months")
          run_function repo_prune_six_months
          break;;
        "Back to main menu")
          return 0;;
        "Quit")
          exit 0;;
        *)
          echo "Try again";;
      esac
    done
  done
}

menu() {
  clear
  split_line
  split_line "Main menu"

  while true; do
    split_line
    PS3="Select your answer: "
    select choice in "Create a snapshot" "Create a patch" "Advanced menu" "Quit"
    do
      split_line
      case $choice in
        "Create a snapshot")
          run_function repo_create_snapshot
          break;;
        "Create a patch")
          run_function repo_create_patch
          break;;
        "Advanced menu")
          run_function advanced_menu
	  break;;
        "Quit")
          exit 0;;
        *)
          echo "Try again";;
      esac
    done
  done
}

#### MAIN ####
run_function check_requirements
run_function repo_init
run_function repo_check
run_function menu
