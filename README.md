# Backup tool

# Requirements

## On source machine

- restic
- parallel

## On destination machine

- tar

# Quick start

1. Edit the .env and specify your value
2. Execute `./launch`
3. Select 1 to make a snapshot
4. Select 2 to make a patch.

This will generate 2 files in the current folder, one bash script and one archive.
The 2 files must not be separated.

5. You could potentially move these 2 files to another computer and run the bash script.

The script will ask you where you want to extract the patch.
You can then iterate this operation.

# Use cases

Creation of a readable archive representing changes between 2 snapshots.
